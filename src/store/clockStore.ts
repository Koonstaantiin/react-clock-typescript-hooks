import React from 'react'
import useGlobalHook from 'use-global-hook'
import { initialState } from '../store/initialState'
import { actions } from '../store/actions'

const useGlobal = useGlobalHook(React, initialState, actions)

export default useGlobal