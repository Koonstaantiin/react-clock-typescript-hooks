import Moment from 'moment'
import { IInitialState } from '../types'

export const initialState: IInitialState = {
  newDate: Moment().toDate()
}