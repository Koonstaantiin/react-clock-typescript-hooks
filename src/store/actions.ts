import Moment from 'moment'

export const actions = {
  updateDate: (store: Storage) => {
    const newDate = Moment().toDate()
    store.setState({ newDate })
  }
}