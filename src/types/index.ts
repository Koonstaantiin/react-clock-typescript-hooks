export interface IInitialState {
  newDate: Date
}

export interface IHotelClock {
  timeOffset?: number
}