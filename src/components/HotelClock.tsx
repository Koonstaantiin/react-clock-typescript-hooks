import React, { useLayoutEffect, useRef } from 'react'
import Moment from 'moment'
import useGlobal from '../store/clockStore'
import { IHotelClock } from '../types'

const HotelClock: React.FC<IHotelClock> = ({timeOffset = 0}) => {
  const [ globalState ] = useGlobal()
  const { newDate } = globalState.newDate

  const refHours   = useRef<SVGRectElement>(null)
  const refMinutes = useRef<SVGRectElement>(null)
  const refSeconds = useRef<SVGLineElement>(null)

  const hotelDate = Moment(newDate).add(timeOffset, 'hours').toDate()

  const hotelDateFormatted = Moment(hotelDate).format('DD.MM.YYYY HH:mm:ss')

  const rotateArrows = () => {
    refSeconds && refSeconds.current && rotateArrow(refSeconds.current, 6 * hotelDate.getSeconds())
    refMinutes && refMinutes.current && rotateArrow(refMinutes.current, 6 * hotelDate.getMinutes())
    refHours && refHours.current && rotateArrow(refHours.current, 30 * (hotelDate.getHours() % 12) + hotelDate.getMinutes() / 2)
  }

  const rotateArrow = (element: Element, degrees: number) => {
    element.setAttribute('transform', 'rotate(' + degrees + ' 50 50)')
  }

  useLayoutEffect(() => {
    rotateArrows()
  })

  return (
    <div className={'clock-wrapper'}>
        <div>Смещение: {timeOffset}</div>
        <div>Время: { hotelDateFormatted }</div>
        <svg className={'clock'} viewBox="0 0 100 100">
          <circle className={'clock-face'} cx="50" cy="50" r="45"/>
          <g className={'arrows'}>
            <rect ref={refHours} className={'arrow-hours'} x="48.5" y="12.5" width="3" height="40" rx="2.5" ry="2.55" />
            <rect ref={refMinutes} className={'arrow-minutes'} x="48" y="12.5" width="3" height="40" rx="2" ry="2"/>
            <line ref={refSeconds} className={'arrow-seconds'} x1="50" y1="50" x2="50" y2="16" />
          </g>
        </svg>
      </div>
  )
}

export default HotelClock