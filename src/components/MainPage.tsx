import React from 'react'
import HotelClock from './HotelClock'

const MainPage: React.FC = () => {
  return (
    <div className="App" id="clockApp">
      <HotelClock />
      <HotelClock timeOffset={5}/>
      <HotelClock timeOffset={-3}/>
    </div>
  )
}

export default MainPage