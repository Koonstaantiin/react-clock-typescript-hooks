import React, { useEffect } from 'react'
import { useRoutes, A } from 'hookrouter'
import useGlobal from '../store/clockStore'
import '../styles/App.scss'
import Routes from '../routes';

const App: React.FC = () => {
  const [globalState, globalActions] = useGlobal()

  useEffect(() => {
    const appTimer = setInterval(() => {
      globalActions.updateDate()
    }, 1000)

    return (() => { clearInterval(appTimer) })
  })

  const routesResult = useRoutes(Routes)

  return (
    <div className="wrapper">
      <div className="links">
        <A href="/">Main page</A>
        <A href="/clock-main">Main clock</A>
        <A href="/clock-plus-5">Timezone +5</A>
        <A href="/clock-sub-3">Timezone -3</A>
      </div>
      {routesResult}
    </div>
  )
}

export default App;
