import React from 'react'
import HotelClock from './components/HotelClock'
import MainPage from './components/MainPage'

const Routes = {
  "/":             () => <MainPage />,
  "/clock-main":   () => <HotelClock />,
  "/clock-plus-5": () => <HotelClock timeOffset={5}/>,
  "/clock-sub-3":  () => <HotelClock timeOffset={-3}/>
}

export default Routes